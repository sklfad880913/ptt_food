#教學
from tkinter import *
from  tkinter  import ttk 
import tkinter as tk
#import pymssql
import pymysql
  

#主頁面
def cancel():
    comboxlist.current('0')
    v.set(0)
    
def request():
    
    db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "ptt",
    "charset": "utf8"
    }
    conn = pymysql.connect(**db_settings)
    cursor = conn.cursor()
    
    #篩選結果子畫面
    win = tk.Toplevel(window)
    win.title('篩選結果')
    l1 = Label(win, text='<篩選條件>')
    l2 = Label(win, text='選擇地區:')
    l3 = Label(win, text='價位區間:')

    r1 = Label(win, text='名稱')
    r2 = Label(win, text='價格')
    r3 = Label(win, text='營業時間')
    r4 = Label(win, text='ptt網址')


    l1.grid(row = 0, column = 0)
    l2.grid(row = 1, column = 0)
    l3.grid(row = 2, column = 0)
    r1.grid(row = 4, column = 1)
    r2.grid(row = 4, column = 2)
    r3.grid(row = 4, column = 3)
    r4.grid(row = 4, column = 4)

    city = comboxlist.get()
    cost = v.get()
    #print(city,v.get())印出篩選資料
    r5 = Label(win, text=city)
    if cost=='0':
        r6 = Label(win, text='無')
    else:
        r6 = Label(win, text=cost)
    r5.grid(row = 1, column = 1)
    r6.grid(row = 2, column = 1)
    
    n=4
    command = ""
    if cost =='0':
        command = "select rst_name,origin_price,time,link from info where location = \'%s\';"%city
    else:
        if cost =="100以下":
            command ="select rst_name,origin_price,time,link from info where location = \'%s\' and avg_price <= 100;" %city
        elif cost =="101~500":
            command ="select rst_name,origin_price,time,link from info where location = \'%s\' and (avg_price <= 500 and avg_price > 100);"%city
        elif cost =="501~1000":
            command = "select rst_name,origin_price,time,link from info where location = \'%s\' and (avg_price <= 1000 and avg_price > 500);"%city
        elif cost =="超過1000":
            command = "select rst_name,origin_price,time,link from info where location = \'%s\' and avg_price > 1000;"%city
    print(command)
    cursor.execute(command) 
    row = cursor.fetchone()  
    while row:
        n+=1
        k1 = Label(win, text=row[0]).grid(row = n, column = 1)
        k2 = Label(win, text=row[1]).grid(row = n, column = 2)
        k3 = Label(win, text=row[2]).grid(row = n, column = 3)
        k4 = Label(win, text=row[3]).grid(row = n, column = 4)
        #print("%s %s"%(row[0],row[1]))    #印出每筆資料(照select的順序印)
        row = cursor.fetchone() 
    conn.close()
    
#主畫面    
window = Tk()
window.title('ptt_food')
window.geometry('300x300')
label1 = Label(window, text='查詢美食餐廳')
label2 = Label(window, text='選擇地區:')
label3 = Label(window, text='平均價位:')

comboxlist=ttk.Combobox(window, state="readonly")  
#comboxlist["values"]=('臺北市','新北市','桃園市','臺中市','臺南市','高雄市','新竹縣','苗栗縣','彰化縣','南投縣','雲林縣','嘉義縣','屏東縣','宜蘭縣','花蓮縣','臺東縣','澎湖縣','金門縣','連江縣','基隆市','新竹市','嘉義市')  
comboxlist["values"]=('台北','新北','桃園','台中','台南','高雄','新竹','苗栗','彰化','南投','雲林','嘉義','屏東','宜蘭','花蓮','台東','澎湖','金門','連江','基隆')
comboxlist.current(0)  

v = tk.StringVar()
v.set('0')

radibut1 = Radiobutton(window, text = "100以下", variable = v, value = "100以下") 
radibut2 = Radiobutton(window, text = "101~500", variable = v, value = "101~500") 
radibut3 = Radiobutton(window, text = "501~1000", variable = v, value = "501~1000") 
radibut4 = Radiobutton(window, text = "超過1000", variable = v, value = "超過1000") 

btn1 = Button(window,text = 'ok', command = request)
btn2 = Button(window,text = 'cancel', command = cancel)

#排版
label1.grid(row = 0, column = 1)
label2.grid(row = 1, column = 0)
comboxlist.grid(row = 1,column = 1)
label3.grid(row = 2, column = 0)
radibut1.grid(row = 2, column = 1, sticky=W)
radibut2.grid(row = 3, column = 1, sticky=W)
radibut3.grid(row = 4, column = 1, sticky=W)
radibut4.grid(row = 5, column = 1, sticky=W)
btn1.grid(row = 6, column = 1, sticky=E)
btn2.grid(row = 6, column = 2, sticky=W)

window.mainloop()
