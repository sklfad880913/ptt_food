"""
This file will scrape ptt_美食版 post content,
and store each field in data[] seperated by "|",
db.py then imports this file and gets the scraped data with get_data().
"""

import requests
from bs4 import BeautifulSoup 
#import connect

def is_all_chinese(strs):
    for _char in strs:
        if not '\u4e00' <= _char <= '\u9fa5':
            return False
        return True

def get_data():
    return data

data = []
total_response = ""
for page in range(6990, 6993):   #選擇要爬的頁數
    url = "https://www.ptt.cc/bbs/Food/index"+str(page)+".html"
    r = requests.get(url)          #將每頁的HTML GET下來
    total_response += r.text
soup = BeautifulSoup(total_response,"html.parser")  #用BeautifulSoup以html格式分析原始碼
sel = soup.select("div.title a")   #選擇tag為title，他的tag a(也就是包含文章標題 & 文章link)
for s in sel:
    #print(s["href"], s.text)    #url, txt 
    if(s.text.find("[食記]")==0):    #用find去找頁面當中標題有【食記】的貼文
        e = requests.get("https://www.ptt.cc" + s["href"])   #用requests模擬使用者行為，向網頁伺服器請求這個網址(其中一篇貼文)的內容
        e_soup = BeautifulSoup(e.text,"html.parser")  
        e_sel = e_soup.select("div.bbs-screen.bbs-content")  #爬下此篇文內文
        link = "https://www.ptt.cc" + s["href"]      #每篇網址的link
        #print(e.text)        #內文
        start_index = e.text.find("地址：")
        if(start_index == -1):  #-1表找不到文中有「地址」這個詞，所以continue直接爬下一篇文
            continue
        else:
            #爬餐廳縣市: rst_addr
            start_index += 3  #從「地址:」冒號後開始抓字串(也就是餐廳地址)
            rst_addr = e.text[start_index:(start_index+2)]   #只爬兩個字(縣市ex.台北)   
            if(is_all_chinese(rst_addr) and (rst_addr != "台灣")):
                rst_addr = rst_addr.replace("臺","台") 
                #print(rst_addr)
            else:
                continue
            

            #餐廳名稱: rst_name
            start_index = e.text.find("名稱")
            if(start_index == -1):
                continue
            else:
                end_index = e.text.find("\n", start_index)  #從「名稱」往後找到\n，也就是名稱的結尾
                rst_name = e.text[(start_index+3):end_index]   #擷取餐廳名稱
                rst_name = rst_name.replace("&amp;", "&")   #將 &amp 轉為 &
                #print(rst_name, rst_addr)
            
                #營業時間: rst_time
                start_index = e.text.find("營業時間")
                if(start_index == -1):
                    continue
                else:
                    end_index = e.text.find("\n", start_index)
                    rst_time = e.text[(start_index+5):end_index]
                    if(len(rst_time)<5):
                        continue
                    else:
                        #print(rst_name, rst_addr, rst_time)
                        
                        #平均價位: rst_price
                        start_index = e.text.find("價位")
                        if(start_index == -1):
                            continue
                        end_index = e.text.find("\n", start_index)
                        rst_price = e.text[(start_index+3):end_index] 
                        
                        #以下處理價位
                        filter_rst_price = ""   #filter過的平均價位
                        rst_price = rst_price.replace("-", "~")  #把所有營業時間裡的"-"取代為"~"，統一化
                        for i in rst_price:
                            if(i.find("~") != -1 or i.isdigit()):  #價格只保留~,number
                                filter_rst_price += i
                        if((len(filter_rst_price) >= 5) and (filter_rst_price.find("~")) == -1):  #價位字串太多且複雜，也沒有"~"(表示不是因為價位區間使字串多) 我就不打算處理
                            filter_rst_price = ""   
                            continue
                        if(filter_rst_price == ""):  #過濾過的價位為空字串
                            continue
                        if(filter_rst_price[-1] == "~"):  #若價位字串最後一字元是"~"，字串就只擷取到~之前
                            filter_rst_price = filter_rst_price[0:-1]
                        if(filter_rst_price.find("~")):
                            price_range = filter_rst_price.split("~")   #價位區間的部分
                            filter_rst_price = int((int(price_range[0])+int(price_range[-1]))/2)  #算平均價位
                        if(len(str(filter_rst_price)) >= 5):
                            continue
                        
                        print(rst_name,"/",rst_addr, "/",rst_price,"/",rst_time,"/",filter_rst_price,"/", link)   #印出餐廳名稱、地區、價位、營業時間、平均價位、連結
                        all_data = rst_name+"|"+rst_addr+"|"+str(filter_rst_price)+"|"+rst_time+"|"+str(rst_price)+"|"+link
                        data.append(all_data)   #存成list
                        

    
    

