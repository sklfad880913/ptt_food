# -*- coding: utf-8 -*-
"""
This file is executed after scraping each ptt_美食版 post content,
especially used for connecting to, and store content by field in db.

"""
#https://www.learncodewithmike.com/2020/02/python-mysql.html

import pymysql
import importlib
my_module = importlib.import_module('ubuntu_1215')


# 資料庫參數設定
db_settings = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "[db_pwd]",
    "db": "ptt",
    "charset": "utf8"
}
try:
    # 建立Connection物件
    conn = pymysql.connect(**db_settings)
    
    # 建立Cursor物件
    with conn.cursor() as cursor:
        # 新增資料SQL語法
        command = "INSERT INTO info(rst_name, location, avg_price, time, origin_price, link)VALUES(%s, %s, %s, %s, %s, %s)"
        datas = my_module.get_data()

        for data in datas:
            arr = data.split("|")
            cursor.execute(command, (arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]))
            
        """
        # reach data to see whether storing correctly
        query_location = "SELECT rst_name, avg_price FROM info WHERE location = %s"
        cursor.execute(query_location, ("台北"))
        results = cursor.fetchall()

        for result in results:
            print("餐廳名: ", result[0], "\t價位: ", result[1])
        """
        # 儲存變更
        conn.commit()
except Exception as ex:
    print(ex)